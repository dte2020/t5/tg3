# T5PROJECTA
# PROGRAMA QUE PERMITE VISUALIZAR DATOS APOYÁNDOSE EN LA LIBRERÍA MATPLOTLIB
# V1.0.2

# librerias necesarias
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.datasets import load_iris
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter
import random


# en este proyecto solo se utilizan dataset personalizados en código o
# aleatorios, por ejemplo este de aquí abajo y mediante numpy, te daría una
# posibilidad aleatoria de un dataset, así podrías trabajar con diferentes ejemplos
# cada vez que quisieras realizar una ejecución
np.random.RandomState(0)

   
def ayuda():
    ''' esta funcion proporcionará un sencill tutorial al usuario '''     
    print ("Bienvenido al prototipo a del grupo T5 de la asignatura desarrollo con tecnologías emergentes del grado de ingeniería de sistemas de información\n")
    
    print("Los creadores de este proyecto han sido: Carlos Palou Ramírez, Samir Majdi Mazhdi, Sergio Cortijo Cano, Iñaki Larumbe Valenciaga y José Rafael Sánchez Hernández\n")
    
    print("En este proyecto podrás, a partir de un conjunto de data sets que te proporcionamos, realizar una visualización de datos en MATPLOTLIB de los mismos, de tal manera ",end='')
    print("que sea posible entender que, sin la visualización de datos, estos al final solo se comportan como números al ojo del ser humano, careciendo de ningún tipo de información útil para nosotros.\n")
   
    print("Para empezar, deberás introducir un nombre de usuario con el que quiera ser tratado el resto del programa. Después, podrás elegir entre un conjunto de opciones dadas en el menú para poder ",end='')
    print("interactuar con ellas: elección de data set, ayuda, repositorio el gitlab, por si te apetece colaborar, notas de la versión y otras opciones que llegarán en el futuro. Una vez hayas elegido un data set",end='') 
    print("con el que te apetezca indagar de una forma más visual, en él podrás elegir el tipo de gráfico que quieras desplegar para obtener la información que más útil desees, por el momento solo disponemos de 5 gráficos,",end='')
    print("esto es unas de las características que añadiremos en el futuro, más gráficos … nuestro dicho es ¡mientras más gráfica más me gratifica! \n")
   
    print("Para terminar este sencillo tutorial, que esperamos que te sirva de ayuda, constatar que se trata de una de las primeras versiones y que en base al feedback que recibamos por vuestra parte seguiremos implementando ",end='')
    print("Nuevas características.\n")
    print("¡Un cordial saludo!\n")
    
    menu()
            
def elegir_opcionvis():
    ''' esta función permite elegir un modo de visualización para un dataset
        ds -> dataset'''
    con=0
    while (con==0):
        try:
            eleccion_menu=int(input("¿Qué visualización deseas probar? \nHISTOGRAMA[1], DIAGRAMA DE CAJA[2], MAPA DE CALOR[3], DIAGRAMA DE VIOLIN[4], DIAGRAMA EN ESPIRAL[5]\n-> "))
            con+=1
            em=eleccion_menu
            if (em==1):
                histograma()
            elif (em==2):
                diagrama_caja()
            elif (em==3):
                mapa_calor()
            elif (em==4):
                diagrama_violin()
            elif (em==5):
                espiral()
            else:
                print ("Introduzca un número que aparezca en el menú\n")
                con-=1
        except:
            print("¡Asegúrate de introducir solo números válidos!\n")
    
    
## CAMBIAR NOMBRE DE VARIABLES Y DESPLAZARLAS, COMENTAR A SER POSIBLE
    
def histograma():
    ''' esta función grafica un histograma simple de distribución elegida
    por el usuario'''
    
    # El usuario puede interaxtuar con el sistema en determinadas funciones
    n_puntos=int(input("¿Cuántos números de puntos aleatorios quieres generar? "))
    n_barras=int(input("¿Cuántos barras en el histograma quieres generar? "))
        
    # Generate a normal distribution, center at x=0 and y=5
    x = np.random.randn(n_puntos)
    y = .4 * x + np.random.randn(n_puntos)
        
    fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True)
        
    # Matplotlib permite ponerle nombre a cada etiqueta y al título de la gráfica
    plt.xlabel('|X|')
    plt.ylabel('|Y|')
    plt.title('¡Esto es un histograma en MATPLOTLIB!')
    
    # graficamos nuestros histogramas
    # primero la X
    axs[0].hist(x, n_barras)
    # segundo la Y
    axs[1].hist(y, n_barras)
    
def diagrama_caja():
    ''' esta funcion grafica un diagrama de caja '''
    # generamos datos aleatorios
    datos = [np.random.normal(0, std, size=100) for std in range(1, 4)]
    # tendremos tres por 
    labels = ['EJ1', 'EJ2', 'EJ3']
    
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(10, 5))
    
    # diagrama de caja
    bplot1 = ax1.boxplot(datos,
                         vert=True,  # graficado vertical
                         patch_artist=True,  # le ponemos color
                         labels=labels)  
    ax1.set_title('Modo regular')
    
    # diagrama de caja estrangulado
    # estrangulado(notch)=true
    bplot2 = ax2.boxplot(datos,notch=True,vert=True,patch_artist=True,labels=labels)  
    ax2.set_title('Modo estrangulado')
    
    # Matplotlib nos permite ajustar nuestro gráfico con colores
    # para facilitar su representacion
    colors = ['pink', 'yellow', 'lightgreen']
    for bplot in (bplot1, bplot2):
        for patch, color in zip(bplot['boxes'], colors):
            patch.set_facecolor(color)
    
    # ponemos nombres a nuestros ejes
    for ax in [ax1, ax2]:
        ax.yaxis.grid(True)
        ax.set_xlabel('Ejemplos')
        ax.set_ylabel('Valores observados')
    
    plt.show()
    
    

def al():
    ''' esta función propociona un número aleatorio para el mapa de calor'''
    x = round(random.uniform(50.50, 500.50), 2)
    return x


def mapa_calor ():
    ''' en esta función generamos un mapa de calor, comparando diferentes coches
    de diferentes compañías y su emisión de CO2 a la atmósfera en toneladas '''
    
    
    coches = ["Coche 1", "Coche 2", "Coche 3", "Coche 4"]
    compañias = ["Wolkswagen", "BMW", "Mercedes","Tesla"]
    
    matriz = np.array([[al(), al(), al(), 0.0],
                        [al(), al(), al(), 0.0],
                        [al(), al(), al(), 0.0],
                        [al(),al(),al(),0.0]])
    
    
    fig, x = plt.subplots()
    im = x.imshow(matriz)
    
    # Metemos los dos arrays en el mismo plot, con la misma longitud,
    # no podríamos hacer esto si difieren en longitud pues no 
    # tendría sentido
    x.set_xticks(np.arange(len(compañias)))
    x.set_yticks(np.arange(len(coches)))
    x.set_xticklabels(compañias)
    x.set_yticklabels(coches)
    
 
    
    # Montamos nuestro mapa
    for i in range(len(coches)):
        for j in range(len(compañias)):
            text = x.text(j, i, matriz[i, j],
                           ha="center", va="center", color="w")
    x.set_title("Toneladas de CO2 volcadas a la atmósfera por coche en un año")
    fig.tight_layout()
    plt.show()
    
    
def diagrama_violin():
    ''' esta función grafica un diagrama de violín, una de las más potentes
    variantes de graficación que ofrece matplotlib, por ello este ejemplo 
    lo hemos obtenido directamente de su documentación, para mostrar matplotlib
    llevado a la máxima potencia posible, con la costumización'''
    
    # obtenemos un dataset aleatorio
    data = [sorted(np.random.normal(0, std, 100)) for std in range(1, 5)]
    
    # establecemos la configuración del plot
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(9, 4), sharey=True)
    
    ax1.set_title('Diagrama de violín')
    ax1.set_ylabel('Valores observados')
    ax1.violinplot(data) # Aquí generamos el plot normal y corriente
    
    
    # ¿Pero y si quisieramos darle un toque especial a la visualización?
    # aquí entramos en el terreno de customización del plot
    ax2.set_title('Mismo diagrama pero customizado')
    parts = ax2.violinplot(
            data, showmeans=False, showmedians=False,
            showextrema=False)
    
    # aplicamos colores
    for pc in parts['bodies']:
        pc.set_facecolor('#D43F3A')
        pc.set_edgecolor('black')
        pc.set_alpha(1)
    
    # elegimos cuartiles, mediana y percentiles
    # caracteristicos también de estos diagramas, como los de caja
    quartile1, medians, quartile3 = np.percentile(data, [25, 50, 75], axis=1)
    whiskers = np.array([
        adjacent_values(sorted_array, q1, q3)
        for sorted_array, q1, q3 in zip(data, quartile1, quartile3)])
    whiskersMin, whiskersMax = whiskers[:, 0], whiskers[:, 1]
    # reajustamos con colores y estilos
    inds = np.arange(1, len(medians) + 1)
    ax2.scatter(inds, medians, marker='o', color='white', s=30, zorder=3)
    ax2.vlines(inds, quartile1, quartile3, color='k', linestyle='-', lw=5)
    ax2.vlines(inds, whiskersMin, whiskersMax, color='k', linestyle='-', lw=1)
    
    # a los ejes le asignamos una definición
    labels = ['A', 'B', 'C', 'D']
    for ax in [ax1, ax2]:
        set_axis_style(ax, labels)
    
    plt.subplots_adjust(bottom=0.15, wspace=0.05)
    # mostramos el resultado final
    plt.show()
    

def espiral():
    ''' esta función grafica varias espirales y con dos ejemplos a elegir '''
    
    em=int(input("¿Espiral normal[1], espiral ´rellena´ [2]? "))
    
    if (em==1):
        r = np.linspace(0.3, 1, 30)
        theta = np.linspace(0, 4*np.pi, 30)
        x = r * np.sin(theta)
        y = r * np.cos(theta)
        
        fig, (espiral_a, espiral_b) = plt.subplots(1, 2, figsize=(6, 3.2))
    
        #Graficamos las dos espirales, con las alternativas que comentamos
        # más abajo, siendo muy útil esto para averiguar puntos clave
        # en una visualización de datos
        espiral_a.plot(x, y, 'C3', lw=3)
        espiral_a.scatter(x, y, s=120)
        espiral_a.set_title('Lineas por encima de puntos')
        
        espiral_b.plot(x, y, 'C3', lw=3)
        espiral_b.scatter(x, y, s=120, zorder=2.5)  # move dots on top of line
        espiral_b.set_title('Puntos por encima de líneas')
        
        plt.tight_layout()

    else:
        # esta función grafica una espiral pero de una manera más decorada
        # en base a unos datos que hemos obtenido de la documentación oficial
        # para hacer un código aleatorio, se puede observar que en vez de crear
        # una mera espiral busca rellenar sus curvas y puntos con colores
        theta = np.arange(0, 8*np.pi, 0.1)
        a = 1
        b = .2
        
        for dt in np.arange(0, 2*np.pi, np.pi/2.0):
        
            # creamos las variables x e y
            x = a*np.cos(theta + dt)*np.exp(b*theta)
            y = a*np.sin(theta + dt)*np.exp(b*theta)
            dt = dt + np.pi/4.0
            # junto a otras
            x2 = a*np.cos(theta + dt)*np.exp(b*theta)
            y2 = a*np.sin(theta + dt)*np.exp(b*theta)
            # las mezclamos en última instacia
            xf = np.concatenate((x, x2[::-1]))
            yf = np.concatenate((y, y2[::-1]))
        
            p1 = plt.fill(xf, yf)
            # graficamos nuestra espiral
            plt.show()
    
    
def adjacent_values(vals, q1, q3):
    ''' esta función ayuda a graficar el diagrama de violín '''
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def set_axis_style(ax, labels):
    ''' esta función ayuda a graficar el diagrama de violín '''
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)
    ax.set_xlabel('Sample name')   
    
    
def repositorio():
    ''' esta funcion proporciona un link para ir al repositorio '''
    print ("Repositorio-> https://gitlab.com/dte2020/t5/tg3\n")    
    menu()
    
def menu():
    ''' Esta función se encarga de gestionar el menú '''
    con=0
    #se utiliza sistema de número-elección como parte de una interfaz sencilla y manejable
    while (con==0):
        try:
            eleccion_menu=int(input("¿Qué deseas hacer hoy? \nSELECCIONAR GRÁFICA[1], AYUDA[2], REPOSITORIO[3], SALIR[4]\n-> "))
            con+=1
            em=eleccion_menu
            if (em==1):
                elegir_opcionvis()
            elif (em==2):
                ayuda()
            elif (em==3):
                repositorio()
            elif (em==4):
                print("Esperemos que hayas disfrutado y aprendido de Matplotlib")
            else:
                print ("Introduzca un número que aparezca en el menú\n")
                con-=1
        except:
            print("¡Asegúrate de introducir solo números válidos!\n")
        

def main():
    
    # el sistema pide al usuario un nombre para tratar con él/ella
    usuario=input("¡Hola! ¿Cómo te llamas? -> ")
    print("Te damos la bienvenida ", usuario,"\n")
    print("Para poder continuar debes introducir SOLO el número de la opción. \n")
    
    menu()
    
if __name__ == '__main__':
    
    df = main()



