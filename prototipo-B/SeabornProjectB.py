# T5PROJECTB
# PROGRAMA QUE PERMITE VISUALIZAR DATOS APOYÁNDOSE EN LA LIBRERÍA SEABORN
# V1.0.1

# librerias necesarias
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
from sklearn.datasets import load_iris
import numpy as np
from matplotlib import colors
from matplotlib.ticker import PercentFormatter


# En el proyecto anterior te ofrecimos el código que permitía obtener dataset
# aleatorios, en este caso te mostramos que también podrías elegir semillas
# específicas para probar o quedarte con un dataset aleatorio que te guste
np.random.seed(10062020)
# cargamos los dataset que nos ofrece SEABORN y/o otras librerías como sklearn
# dataset de cuentas de restaurante
tips = sb.load_dataset("tips")
# dataset de estudio en hojas de tres plantas
iris = sb.load_dataset("iris")
# dataset de vuelos
vuelos= sb.load_dataset("flights")
sb.set()


def ayuda():
    ''' esta funcion proporcionará un sencill tutorial al usuario '''     
    print ("Bienvenido al prototipo a del grupo T5 de la asignatura desarrollo con tecnologías emergentes del grado de ingeniería de sistemas de información\n")
    
    print("Los creadores de este proyecto han sido: Carlos Palou Ramírez, Samir Majdi Mazhdi, Sergio Cortijo Cano, Iñaki Larumbe Valenciaga y José Rafael Sánchez Hernández\n")
    
    print("En este proyecto podrás, a partir de un conjunto de data sets que te proporcionamos, realizar una visualización de datos en SEABORN de los mismos, de tal manera ",end='')
    print("que sea posible entender que, sin la visualización de datos, estos al final solo se comportan como números al ojo del ser humano, careciendo de ningún tipo de información útil para nosotros.\n")
   
    print("Para empezar, deberás introducir un nombre de usuario con el que quiera ser tratado el resto del programa. Después, podrás elegir entre un conjunto de opciones dadas en el menú para poder ",end='')
    print("interactuar con ellas: elección de data set, ayuda, repositorio el gitlab, por si te apetece colaborar, notas de la versión y otras opciones que llegarán en el futuro. Una vez hayas elegido un data set",end='') 
    print("con el que te apetezca indagar de una forma más visual, en él podrás elegir el tipo de gráfico que quieras desplegar para obtener la información que más útil desees, por el momento solo disponemos de 5 gráficos,",end='')
    print("esto es unas de las características que añadiremos en el futuro, más gráficos … nuestro dicho es ¡mientras más gráfica más me gratifica! \n")
   
    print("Para terminar este sencillo tutorial, que esperamos que te sirva de ayuda, constatar que se trata de una de las primeras versiones y que en base al feedback que recibamos por vuestra parte seguiremos implementando ",end='')
    print("Nuevas características.\n")
    print("¡Un cordial saludo!\n")
    
    menu()

            
def elegir_opcionvis():
    ''' esta función permite elegir un modo de visualización para un dataset
        ds -> dataset'''
    con=0
    while (con==0):
        try:
            eleccion_menu=int(input("¿Qué visualización deseas probar? \nHISTOGRAMA[1], DIAGRAMA DE CAJA[2], MAPA DE CALOR[3], DIAGRAMA DE VIOLIN[4], DIAGRAMA DE ESPIRAL[5]\n-> "))
            con+=1
            em=eleccion_menu
            if (em==1):
                histograma()
            elif (em==2):
                diagrama_caja()
            elif (em==3):
                mapa_calor()
            elif (em==4):
                diagrama_violin()
            elif (em==5):
                espiral()
            else:
                print ("Introduzca un número que aparezca en el menú\n")
                con-=1
        except:
            print("¡Asegúrate de introducir solo números válidos!\n")
    
    
## CAMBIAR NOMBRE DE VARIABLES Y DESPLAZARLAS, COMENTAR A SER POSIBLE
    
def histograma():
    ''' esta función grafica un histograma que compara el tiempo y el gasto 
    para comida y cena en el sexo masculino y femenino '''
    
    
    # existen múltiples variantes para generar una estilización más personal
    # acerca del visualizado de datos, por ejemplo: dark, ticks, whitegrid ...
    sb.set(style="whitegrid")
    
    # facetgrid se utiliza para conjuntar todos los datos a la hora de hacer
    # su visualización, por ejemplo en este caso establecemos dos "ejes" que
    # son sexo por un lado y tiempo por el otro
    # ¡OJO! Tienen que ser nombres dentro del dataset, no podemos ponerlos
    # como queramos, sino tal y como se presentan en el dataset
    g = sb.FacetGrid(tips, row="sex", col="time", margin_titles=True)
    # creamos una distribución 
    bins = np.linspace(0, 60, 13)
    g.map(plt.hist, "total_bill", color="steelblue", bins=bins)
    
def diagrama_caja():
    ''' esta función grafica un diagrama de caja acerca de la forma de los 
    pétalos y el número de unidades relacionados a cada campo, de forma que
    al existir múltiples variables podemos mejor un análisis de cuartiles,
    percentiles y excepciones en cada ejemplo'''
    
    # Como se puede ver en algunas funciones solo necesitan de una linea de código
    # para realizar un gráfico medianamente complejo
    # en esta función metemos nuestro dataset, orientación y paleta de colores
    
    em=input("Orientación vertical[v] u horizontal [h]: ")
    if (em=="v"):
        sb.boxplot(data=iris, orient="v", palette="Paired")
    else:
        sb.boxplot(data=iris, orient="h", palette="Paired")
    
    
    # El código de abajo muestra una ejecución más compleja del diagrama de caja
    # si quieres probarlo solo descoméntalo apartir de sb.boxplot
    # sb.boxplot(x="day", y="total_bill",
    #         hue="smoker", palette=["m", "g"],
    #         data=tips)
    # sb.despine(offset=10, trim=True)
    

def mapa_calor ():
    ''' esta función grafica un mapa de calor, es decir, una gráfica en la que 
    a diversas variables se les aplica un peso y en función de este en el máximo
    y mínimo peso global se le asigna un color'''
    
    
    
    em=int(input("¿Te apetece ver un mapa de calor sobre longitud de pétalos[1](Esto es una versión mejorada), sobre vuelos comerciales[2], mapa de calor versión simplificada [3]? "))
    
    if (em==1):
        # esto es un mapa de calor dentro de mapas de calores, lo cual es algo realmente
        # grande, como todo lo concerniente a iris estudia tamaño de pétalos y flores
        # puede suponer un problema de graficación dependiendo de la potencia de tu equipo
        sb.set(color_codes=True)
        species = iris.pop("species")
        g = sb.clustermap(iris)
    elif (em==2):
        # por otro lado mostramos el mapa de calor que hemos utilizado alguna
        # vez este curso y es de pasajeros por año y mes en vuelos comerciales
        # una gráfica muy rica e interesante
        
        # el . pivot lo utilizamos para orientar un dataset normal hacia un mapa de calor
        # seguimos trabajando con los nombres QUE se incluyen en el dataset, no se 
        # pueden modificar
        hm = vuelos.pivot("month", "year", "passengers")
    
        # Establacemos el tamaño de visualizado y otras opciones
        f, ax = plt.subplots(figsize=(9, 6))
        sb.heatmap(hm, annot=True, fmt="d", linewidths=.5, ax=ax)
    else:
        # mapa de calor simple generado con datos aleatorios
        # para que el usuario pueda ver cómo funciona
        np.random.seed(12345)
        datos_aleatorios = np.random.rand(12, 14)
        sb.heatmap(datos_aleatorios)
    
    

    
    
    
def diagrama_violin():
    ''' esta funcion grafica un diagrama de violin, muy utiliza para
    mostrar múltiples distribuciones de datos a la vez'''


    em=int(input("¿Te apetece aplicarlo sobre longitud de pétalos[1] o algo más simple y aleatorio [2]? "))
    
    if (em==1):
        # una simle línea permite obtener un gráfico muy potente y visual,
        # una característica de seaborn ... ¡pruébalo!
        # sepal_length
        sb.violinplot(x='species', y='sepal_length', data=iris, order=[ "versicolor", "virginica", "setosa"])
        # las especies Iris setosa, Iris virginica e Iris versicolor, son las que utiliza
        # por defecto ese dataset, por eso aplicamos un cierto orden a la visualización,
        # pero no podemos establacer nuestro propio nombre, si no que debe estar
        # presente en el dataset

    else:
        # Creamos una distribución aleatoria
        n, p = 40, 8
        d = np.random.RandomState(12345).normal(0, 2, (n, p))
        d += np.log(np.arange(1, p + 1)) * -5 + 10
        
        # cubehelix se utiliza para darle una paleta de color a la visualización
        pal = sb.cubehelix_palette(p, rot=-.5, dark=.3)
        
        # grafica el diagrama acompañado de puntos para facilitar su visualizado
        sb.violinplot(data=d, palette=pal, inner="points")
    

def espiral():
    ''' esta funcion grafica tres espirales a partir de un dataset
    conviertiendo este '''

    # generado un dataset aleatorio
    ejemplo_dataset = np.linspace(0, 10, num=100)
    # y graficamos tres versiones diferentes del mismo dataset y creamos un dataframe
    df = pd.DataFrame({'r': ejemplo_dataset, 'slow': ejemplo_dataset, 'medium': 2 * ejemplo_dataset, 'fast': 4 * ejemplo_dataset})
    
    # ajustamos nuestro dataframe con diferentes variables
    df = pd.melt(df, id_vars=['r'], var_name='speed', value_name='theta')
    
    # ajustamos el facetgrid que explicamos en el histograma
    g = sb.FacetGrid(df, col="speed", hue="speed",
                      subplot_kws=dict(projection='polar'), height=4.5,
                      sharex=False, sharey=False, despine=False)
    
    # dibujamos con el scatterplot
    g.map(sb.scatterplot, "theta", "r")
    
    
    
def repositorio():
    ''' esta funcion proporciona un link para ir al repositorio '''
    print ("Repositorio-> https://gitlab.com/dte2020/t5/tg3\n")    
    
    menu()
    
def menu():
    ''' Esta función se encarga de gestionar el menú '''
    con=0
    #se utiliza sistema de número-elección como parte de una interfaz sencilla y manejable
    while (con==0):
        try:
            eleccion_menu=int(input("¿Qué deseas hacer hoy? \nSELECCIONAR GRÁFICA[1], AYUDA[2], REPOSITORIO[3], SALIR[4]\n-> "))
            con+=1
            em=eleccion_menu
            if (em==1):
                elegir_opcionvis()
            elif (em==2):
                ayuda()
            elif (em==3):
                repositorio()
            elif (em==4):
                print("¡Esperemos que hayas disfrutado y aprendido de Seaborn!")
            else:
                print ("Introduzca un número que aparezca en el menú\n")
                con-=1
        except:
            print("¡Asegúrate de introducir solo números válidos!\n")
        

def main():
    
    # el sistema pide al usuario un nombre para tratar con él/ella
    usuario=input("¡Hola! ¿Cómo te llamas? -> ")
    print("Te damos la bienvenida ", usuario,"\n")
    print("Para poder continuar debes introducir SOLO el número de la opción. \n")
    
    menu()
    
if __name__ == '__main__':
    
    df = main()
